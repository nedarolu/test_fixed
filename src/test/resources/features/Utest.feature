
@stories
Feature:  As a user, I want to register in utest
  @scenario1
  Scenario Outline:   Register in utest
    Given That  Michael accesses the registration page

    When The   enters the requested data
      |strFirstName  | strLastName   | strEmail    | strMonth   | strDay   | strYear  | strPassword  | strWelcome |
      |<strFirstName>| <strLastName>  | <strEmail> | <strMonth> | <strDay> | <strYear> | <strPassword>  | <strWelcome> |
    Then The search requires entering personal data
      | strWelcome |
      | <strWelcome> |

    Examples:
      |  strFirstName | strLastName  | strEmail        | strMonth       | strDay      | strYear   | strPassword | strWelcome |
      |  Jorge        | Sanz         |  sanz@gmail.com |   number:11    | number:8    | number:1992  | Trkne2gikJ*l  | Welcome to the world's largest community of freelance software testers! |


