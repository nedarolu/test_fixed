package co.com.choucair.test.stepdefinitions;



import co.com.choucair.test.tasks.OpenUp;
import co.com.choucair.test.model.UtestData;
import co.com.choucair.test.tasks.Signup;
import co.com.choucair.test.questions.Answer;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;


public class UtestStepDefinitions {

    @Before
    public void setStage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^That  Michael accesses the registration page$")
    public void thatMichaelAccessesTheRegistrationPage() {
        OnStage.theActorCalled( "Michael").wasAbleTo(OpenUp.thePage());
    }


    @When("^The   enters the requested data$")
    public void theEntersTheRequestedData(List<UtestData> utestData) {
        OnStage.theActorInTheSpotlight().attemptsTo(Signup.onTheSignup(utestData.get(0).getStrFirstName(),
                utestData.get(0).getStrLastName(),utestData.get(0).getStrEmail(),utestData.get(0).getStrMonth(),
                utestData.get(0).getStrDay(),utestData.get(0).getStrYear(),utestData.get(0).getStrPassword()));

    }

    @Then("^The search requires entering personal data$")
    public void theSearchRequiresEnteringPersonalData(List<UtestData> utestData) {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.toThe(utestData.get(0).getStrWelcome())));

    }




}
