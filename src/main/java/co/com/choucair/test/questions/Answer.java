package co.com.choucair.test.questions;

import co.com.choucair.test.userinterface.SignupUtest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class Answer implements Question<Boolean> {
    private String question;

    public Answer(String question) {
        this.question = question;
    }

    public static Answer toThe (String question){

        return  new Answer(question);

    }


    @Override
    public Boolean answeredBy(Actor actor) {
        boolean result;
        String nameEnd= Text.of(SignupUtest.LABEL_END).viewedBy(actor).asString();
        if(question.equals(nameEnd)){
            result=true;
        }
        else{
            result=false;
        }
        return result;
    }
}
