package co.com.choucair.test.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import co.com.choucair.test.userinterface.SearchUtest;
import co.com.choucair.test.userinterface.SignupUtest;
import net.serenitybdd.screenplay.actions.SelectFromOptions;



public class Signup implements Task  {

    private String strFirstName;
    private String strLastName;
    private String strEmail;
    private String strMonth;
    private String strDay;
    private String strYear;
    private String strPassword;





    public Signup(String strFirstName,String strLastName,String strEmail,String strMonth,String strDay,String strYear, String strPassword) {
        this.strFirstName = strFirstName;
        this.strLastName = strLastName;
        this.strEmail= strEmail;
        this.strMonth= strMonth;
        this.strDay= strDay;
        this.strYear= strYear;
        this.strPassword= strPassword;




    }




    public static Signup onTheSignup(String strFirstName,String strLastName,String strEmail,String strMonth,String strDay,String strYear, String strPassword) {
        return Tasks.instrumented(Signup.class,strFirstName,strLastName,strEmail,strMonth,strDay,strYear, strPassword);
    }





    @Override
    public  <T extends Actor> void performAs(T actor){

        actor.attemptsTo(Click.on(SearchUtest.BUTTON_SINGUP),
                Enter.theValue(strFirstName).into(SignupUtest.INPUT_FIRSNAME),
                Enter.theValue(strLastName).into(SignupUtest.INPUT_LASTNAME),
                Enter.theValue(strEmail).into(SignupUtest.INPUT_EMAIL),
                SelectFromOptions.byValue(strMonth).from(SignupUtest.SELECT_MONTH),
                SelectFromOptions.byValue(strDay).from(SignupUtest.SELECT_DAY),
                SelectFromOptions.byValue(strYear).from(SignupUtest.SELECT_YEAR),
                Click.on(SignupUtest.ENTER_BUTTON_LOCATION),
                //Enter.theValue("Bogotá, Bogota, Colombia ").into(SignupUtest.INPUT_CITY),


                Click.on(SignupUtest.ENTER_BUTTON_DEVICES),
                Click.on(SignupUtest.INPUT_BUTTON_LASTSTEP),
                Enter.theValue(strPassword).into(SignupUtest.INPUT_PASSWORD),
                Enter.theValue(strPassword).into(SignupUtest.INPUT_CONFIRMPASSWORD),
                Click.on(SignupUtest.CHECK_TERMINOS),
                Click.on(SignupUtest.CHECK_privacity),
                Click.on(SignupUtest.INPUT_BUTTON_COMPLETED)











               // Enter.theValue("Fusagasugá").into(SignupUtest.INPUT_CITY_LOCATION),
                //Click.on(SignupUtest.INPUT_CITY_LOCATION),
               // Enter.theValue("111311").into(SignupUtest.INPUT_ZIP_LOCATION),
                //Click.on(SignupUtest.ENTER_BUTTON_DEVICES)

                );







    }
}
