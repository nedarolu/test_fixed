package co.com.choucair.test.model;

import lombok.Data;

@Data
public class UtestData {

    private String strFirstName;
    private String strLastName;
    private String strEmail;
    private String strMonth;
    private String strDay;
    private String strYear;
    private String strWelcome;
    private String strPassword;


}
